# Адресная книга
## Аннотация

<b>В разделе доступны</b>:

1. Просмотр информации в адресной книге по маршрутам;
2. Поиск IATA-адресов;

## 1. Просмотр информации в адресной книге

В разделе отображаются адреса с разрешением отображения в адресной книге по всем Клиентам ИС АСП.

<b>Отображаемые данные</b>: 

 - IATA-адрес
 - Владелец
 - Назначение 
 - Город/аэропорт
 - Статус доставки

## 2. Поиск IATA-адресов в Адресной книге

<b>Необходимо</b>:

 1. Ввести данные в поисковую строку
 2. Нажать на кнопку «Найти». Результат поиска отобразится на экране.
